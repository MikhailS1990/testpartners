<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referal extends Model
{
    const FIRST_LEVEL = 1;

    const SECOND_LEVEL = 2;

    const THIRD_LEVEL = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'ref_id', 'level', 'ref_token'
    ];

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    /**
     * Get the partner.
     */
    public function partner()
    {
        return $this->belongsTo('App\User','ref_id');
    }

    /**
     * Create referal and add points
     *
     * @param  int $user_id
     * @param  User $partner
     * @param  int $level
     * @return bool
     */
    public static function createReferal($user_id,$partner,$level)
    {
        self::create([
            'user_id' => $user_id,
            'ref_id' => $partner->id,
            'level' => $level
        ]);
        $partner->points = intval($partner->points) + User::POINTS[$level-1];
        return $partner->save();
    }
}
