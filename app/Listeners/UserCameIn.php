<?php

namespace App\Listeners;

use App\Events\UserOnline;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCameIn
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserOnline  $event
     * @return void
     */
    public function handle(UserOnline $event)
    {
        //
    }
}
