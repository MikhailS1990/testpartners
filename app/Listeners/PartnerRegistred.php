<?php

namespace App\Listeners;

use App\Events\RegistredPartners;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PartnerRegistred
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegistredPartners  $event
     * @return void
     */
    public function handle(RegistredPartners $event)
    {
        //
    }
}
