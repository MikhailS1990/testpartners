<?php

namespace App\Http\Controllers;

use App\User;
use App\Referal;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'desc')->paginate(15);
        $partners = [];
        foreach ($users as $user) {
            $firstLevel = $user->referals()->where('level', Referal::FIRST_LEVEL)->first();
            $secondLevel = $user->referals()->where('level', Referal::SECOND_LEVEL)->first();
            $thirdLevel = $user->referals()->where('level', Referal::THIRD_LEVEL)->first();
            $partners[$user->id] = [
                $firstLevel ? $firstLevel->partner()->first()->name: null ,
                $secondLevel ? $secondLevel->partner()->first()->name: null,
                $thirdLevel ? $thirdLevel->partner()->first()->name: null
            ];
        }
        return view('home', ['users' => $users,'partners' => $partners]);
    }
}
