@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Рейтинг</div>

                <div class="panel-body">
                    <div class="container">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Логин</th>
                                    <th>Количетво очков</th>
                                    <th>Партнер 1 уровня</th>
                                    <th>Партнер 2 уровня</th>
                                    <th>Партнер 3 уровня</th>
                                    <th>Дата регистрации</th>
                                    <th>Дата обновления</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{intval($user->points)}}</td>
                                    <td>{{$partners[$user->id][0]}}</td>
                                    <td>{{$partners[$user->id][1]}}</td>
                                    <td>{{$partners[$user->id][2]}}</td>
                                    <td>{{$user->created_at}}</td>
                                    <td>{{$user->updated_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
